// count operator
db.fruits.aggregate([
        { $match : { onSale : true } },
        { $count: { FruitsOnSale: { $sum : "$onSale" } } }
    ]);


// total number of fruits
db.fruits.aggregate([
        { $match : { stock : { $gte: 20} } },
        { $group : { enoughtStock: { $count: "$stock" } } }
    ]);

// avg_price
db.fruits.aggregate([
        { $match : { onSale : true } },
        { $group: { _id: "$supplier_id", avg_price: { $avg : "$price" } } }
    ]);

// max_price
db.fruits.aggregate([
        { $match : { onSale : true } },
        { $group: { _id: "$supplier_id", max_price: { $max : "$price" } } }
    ]);

// min_price
db.fruits.aggregate([
        { $match : { onSale : true } },
        { $group: { _id: "$supplier_id", max_price: { $min : "$price" } } }
    ]);